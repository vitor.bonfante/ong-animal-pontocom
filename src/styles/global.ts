import styled, { createGlobalStyle } from 'styled-components'
import image from 'next/image'
import Preview from '../../public/web_preview.png'

export default createGlobalStyle`
  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    scroll-behavior: smooth;
  }

  body {
    background-color: #fff;
    font-family: 'Poppins', sans-serif;

  }

  main{
    overflow-x: hidden;
    @media (max-width: 1081px){
      width: 100vw;
      height: 100%;
    }
  }

  :root{
    --color-blue:#009BAE;
    --color-yellow:#FFB401;
    --color-white:#F0F0F0;
    --color-brown: #402D00;
  }
`
