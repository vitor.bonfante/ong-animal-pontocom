import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  padding-top: 50px;
  padding-bottom: 70px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--color-white);

  .title {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 50px;
    line-height: 75px;
    color: var(--color-blue);
    margin-bottom: 15px;
  }

  .sub {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    line-height: 36px;
    color: var(--color-blue);
    margin-top: 35px;
    text-align: center;
  }
`
export const Content = styled.div`
  max-width: 1100px;
  width: 100%;

  @media (max-width: 1080px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-left: 12px;
    padding-right: 12px;
  }
`

export const Images = styled.div`
  display: flex;
  justify-content: center;
  gap: 80px;
`
