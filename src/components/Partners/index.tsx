import Image from 'next/image'

import P1 from '../../../public/Partners/1.png'
import P2 from '../../../public/Partners/2.png'

import { Container, Content, Images } from './styles'

const Partners: React.FC = () => {
  return (
    <Container>
      <Content>
        <p className="title">Parceiros</p>
        <Images>
          <Image src={P1} objectFit="contain" />
          <Image src={P2} objectFit="contain" />
        </Images>
        <p className="sub">Apoie nossa causa, seja um parceiro também!</p>
      </Content>
    </Container>
  )
}

export default Partners
