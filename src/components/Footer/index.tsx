import React from 'react'
import {
  Address,
  Container,
  Donate,
  ImageContainer,
  InfosContainer,
  Logo,
  Phone,
  Social
} from './styles'
import Image from 'next/image'
import { BsInstagram, BsFacebook } from 'react-icons/bs'
import { FaPhoneAlt } from 'react-icons/fa'
import { MdPinDrop } from 'react-icons/md'

import LogoImg from '../../../public/Header/logo-header.png'
import Pix from '../../../public/Header/pix.svg'

const Footer: React.FC = () => {
  return (
    <Container id="donate">
      <Logo>
        <ImageContainer>
          <Image src={LogoImg} objectFit="contain" />
        </ImageContainer>
      </Logo>
      <InfosContainer>
        <Social>
          <p>Fale Conosco</p>
          <div>
            <BsInstagram
              color="var(--color-white)"
              size={38}
              style={{ marginRight: 12, cursor: 'pointer' }}
              onClick={() => {
                window.open('https://www.instagram.com/amigofielong/', '_blank')
              }}
            />
            <BsFacebook
              color="var(--color-white)"
              size={38}
              style={{ cursor: 'pointer' }}
              onClick={() => {
                window.open('https://www.facebook.com/amigofielforq/', '_blank')
              }}
            />
          </div>
        </Social>
        <Phone>
          <FaPhoneAlt
            color="var(--color-yellow)"
            size={52}
            style={{ marginBottom: 16 }}
          />
          <p>Entre em contato</p>
          <p>via direct no</p>
          <p>Instagram</p>
        </Phone>
        <Address>
          <MdPinDrop color="var(--color-yellow)" size={52} />
          <p>Forquilhinha - SC</p>
        </Address>
      </InfosContainer>
      <Donate>
        <p>Caixa AG 3415, c/c</p>
        <p>560-5, Op 003</p>
        <p>CNPJ 26.185-656/0001-10 </p>
      </Donate>
    </Container>
  )
}

export default Footer
