import styled from 'styled-components'

export const Container = styled.footer`
  width: 100%;
  z-index: 10;
  background-color: var(--color-blue);
  display: flex;
  justify-content: center;
  padding: 46px 0px;
  gap: 48px;
  @media (max-width: 1081px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 32px;
  }
`
export const ImageContainer = styled.div`
  width: 191px;
  height: 100%;
  display: flex;
`

export const InfosContainer = styled.div`
  display: flex;
  gap: 48px;
  @media (max-width: 1081px) {
    flex-direction: column-reverse;
    justify-content: center;
    align-items: center;
  }
`

export const Logo = styled.div``

export const Social = styled.div`
  > p {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 29px;
    line-height: 38px;
    text-align: right;
    text-transform: capitalize;
    color: var(--color-yellow);
    margin-bottom: 30px;
  }
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Phone = styled.div`
  > p {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 23px;
    text-align: right;
    color: var(--color-white);
  }
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  @media (max-width: 1081px) {
    justify-content: center;
    align-items: center;
  }
`

export const Address = styled.div`
  > p {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 23px;
    text-align: right;
    letter-spacing: -0.05em;
    color: var(--color-white);
    text-transform: capitalize;
    max-width: 293px;
    margin-top: 16px;
  }
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  @media (max-width: 1081px) {
    justify-content: center;
    align-items: center;
  }
`

export const Donate = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  > p {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    color: #ffffff;
  }
`
