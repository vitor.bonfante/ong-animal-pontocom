import styled from 'styled-components'

export const Background = styled.div`
  background-color: var(--color-blue);
  margin-top: -8px;
  display: flex;
  flex-direction: row-reverse;
  align-items: center;
  justify-content: center;

  .title {
    width: 100%;
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 50px;
    line-height: 75px;
    color: var(--color-white);
  }

  .content {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    text-align: justify;
    color: var(--color-white);
  }

  .button-text {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    color: var(--color-white);

    line-height: 36px;
  }
  @media (max-width: 1080px) {
    flex-direction: column-reverse;
    align-items: center;
    justify-content: center;
  }
`

export const ImageContainer = styled.div`
  width: 829px;
  margin-top: -54px;
  @media (max-width: 1080px) {
    margin-top: 0px;
    margin-left: 0px;
    width: 400px;
  }
`

export const Left = styled.div`
  margin-right: -250px;
  @media (max-width: 1080px) {
    margin-right: 0px;
  }
`

export const Right = styled.div`
  max-width: 450px;
  margin-right: 62px;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-left: 12px;
  padding-right: 12px;

  > p {
    margin-bottom: 5px;
  }

  > a {
    text-decoration: none;
    margin-top: 5px;
    padding: 6px 24px;
    border-radius: 500px;
    background-color: var(--color-blue);
    border: 2px solid var(--color-white);
    cursor: pointer;
  }

  @media (max-width: 1080px) {
    margin-right: 0px;
  }
`
