import Image from 'next/image'

import Racao from '../../../public/Animals/racao.png'

import { Background, Left, Right, ImageContainer } from './styles'

const RightDiv: React.FC = () => {
  return (
    <Background id="volunteering">
      <Left>
        <ImageContainer>
          <Image src={Racao} />
        </ImageContainer>
      </Left>
      <Right>
        <p className="title">Sobre a ONG</p>
        <p className="content">
          Somos uma organização de proteção animal não governamental do
          município de Forquilhinha, formada por pessoas que tem como missão
          ajudar os animais contra os maus-tratos, fome, situações de rua e
          abandono. Trabalhamos sem fins lucrativos, apenas com doações para os
          animais. Venha fazer parte dessa causa!
        </p>
        <a target="_blank" href="https://www.instagram.com/amigofielong/">
          <p className="button-text">Quero fazer parte!</p>
        </a>
      </Right>
    </Background>
  )
}

export default RightDiv
