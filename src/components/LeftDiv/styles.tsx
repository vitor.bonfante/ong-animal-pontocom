import styled from 'styled-components'

export const Background = styled.div`
  background-color: var(--color-yellow);
  display: flex;
  align-items: center;
  justify-content: center;

  .title {
    width: 100%;
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 50px;
    line-height: 75px;
    color: var(--color-brown);

    @media (max-width: 1080px) {
      font-size: 35px;
    }
  }

  .content {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    text-align: justify;
    color: var(--color-brown);
  }

  .button-text {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    color: var(--color-brown);

    line-height: 36px;

    @media (max-width: 1080px) {
      font-size: 20px;
    }
  }

  @media (max-width: 1080px) {
    flex-direction: column-reverse;
    align-items: center;
    justify-content: center;
  }
`

export const ImageContainer = styled.div`
  width: 564px;
  margin-top: -94px;
  @media (max-width: 1080px) {
    margin-top: 0px;
    width: 252px;
  }
`

export const Left = styled.div`
  margin-right: 181px;
  @media (max-width: 1080px) {
    margin: 0px;
  }
`

export const Right = styled.div`
  max-width: 470px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;

  > p {
    margin-bottom: 5px;
  }

  > a {
    text-decoration: none;
    margin-top: 5px;
    padding: 6px 24px;
    border-radius: 500px;
    background-color: var(--color-yellow);
    border: 2px solid var(--color-brown);
    cursor: pointer;
  }
`
