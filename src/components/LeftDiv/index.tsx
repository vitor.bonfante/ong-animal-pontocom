import Image from 'next/image'

import Cat from '../../../public/Animals/gatinho.png'

import { Background, Left, Right, ImageContainer } from './styles'

const LeftDiv: React.FC = () => {
  return (
    <Background>
      <Left>
        <ImageContainer>
          <Image src={Cat} />
        </ImageContainer>
      </Left>
      <Right>
        <p className="title">Apoie essa causa</p>
        <p className="content">
          Somos uma organização sem fins lucrativos e por isso precisamos de sua
          ajuda. Com a sua doação podemos continuar resgatando os animais de rua
          de situações de perigo, além de cuidar e ajudar com medicamentos,
          cirurgias e alimentação. Graças a você podemos transformar o mundo dos
          animais em um lugar melhor!
        </p>
        <a target="_blank" href="https://www.instagram.com/amigofielong/">
          <p className="button-text">Quero ajudar os animais!</p>
        </a>
      </Right>
    </Background>
  )
}

export default LeftDiv
