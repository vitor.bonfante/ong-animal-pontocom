import Image from 'next/image'
import { BsInstagram, BsFacebook } from 'react-icons/bs'
import { BiMenu } from 'react-icons/bi'
import { CgClose } from 'react-icons/cg'

import {
  HeaderContainer,
  HeaderContent,
  HeaderItens,
  HeaderItemMobile,
  ImageContainer,
  ContainerMobile,
  SideMenuSocial
} from './styles'

import Logo from '../../../public/Header/logo-header.png'
import Pix from '../../../public/Header/pix.svg'

const Header: React.FC = () => {
  function openMenu() {
    const menu = document.getElementById('menu')
    menu?.classList.toggle('active')
  }

  return (
    <>
      <ContainerMobile id="menu">
        <CgClose
          color="#FFF"
          style={{ alignSelf: 'flex-end', marginRight: 28 }}
          size={34}
          onClick={openMenu}
        />
        <a href="#">HOME</a>
        <a href="#about">SOBRE NÓS</a>
        <a href="#volunteering">VOLUNTARIADO</a>
        <a href="#donate">DOAR</a>
        <SideMenuSocial>
          <a href="https://www.instagram.com/amigofielong/" target="_blank">
            <BsInstagram
              color="var(--color-white)"
              size={22}
              style={{ marginRight: 12 }}
            />
          </a>
          <a
            onClick={() => {
              window.open('https://www.facebook.com/amigofielforq/', '_blank')
            }}
          >
            <BsFacebook color="var(--color-white)" size={22} />
          </a>
        </SideMenuSocial>
      </ContainerMobile>
      <HeaderContainer>
        <HeaderContent>
          <ImageContainer>
            <Image src={Logo} />
          </ImageContainer>
          <HeaderItens>
            <ul>
              <li>
                <a href="#">HOME</a>
              </li>
              <li>
                <a href="#about">SOBRE NÓS</a>
              </li>
              <li>
                <a href="#volunteering">VOLUNTARIADO</a>
              </li>
              <li>
                <a href="#donate">DOAR</a>
              </li>
              <li
                onClick={() => {
                  window.open(
                    'https://www.instagram.com/amigofielong/',
                    '_blank'
                  )
                }}
              >
                <BsInstagram color="var(--color-white)" size={32} />
              </li>
              <li
                onClick={() => {
                  window.open(
                    'https://www.instagram.com/amigofielong/',
                    '_blank'
                  )
                }}
              >
                <BsFacebook color="var(--color-white)" size={32} />
              </li>
            </ul>
          </HeaderItens>
          <HeaderItemMobile>
            <BiMenu size={34} color="var(--color-white)" onClick={openMenu} />
          </HeaderItemMobile>
        </HeaderContent>
      </HeaderContainer>
    </>
  )
}

export default Header
