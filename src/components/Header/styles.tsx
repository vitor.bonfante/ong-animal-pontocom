import styled from 'styled-components'

export const HeaderContainer = styled.header`
  width: 100%;
  height: 130px;
  display: flex;
  align-items: center;
  background-color: var(--color-blue);
  box-shadow: 0px 11px 38px -4px rgba(0, 0, 0, 0.21);
  padding: 0px 80px;

  @media (max-width: 1080px) {
    height: 84px;
    padding: 0px 40px;
  }
`

export const HeaderContent = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`

export const HeaderItens = styled.div`
  display: flex;
  align-items: center;
  > ul {
    display: flex;
    gap: 35px;
    list-style-type: none;
    > li {
      border-radius: 500px;
      transition: 0.2s ease-in-out;
      cursor: pointer;
      display: flex;
      align-items: center;
      a {
        padding: 0px 12px;
        text-decoration: none;
        font-style: normal;
        font-weight: 500;
        font-size: 22px;
        line-height: 33px;
        color: var(--color-white);
      }

      :hover {
        background-color: var(--color-yellow);
      }

      :nth-child(4) {
        background-color: var(--color-yellow);
        color: var(--color-blue);
        :hover {
          a {
            transition: 0.2s ease-in-out;
            color: var(--color-blue);
          }
        }
      }

      :nth-child(5) {
        padding: 0px 12px;
      }
      :last-child {
        padding: 0px 12px;
      }
    }
  }

  @media (max-width: 1080px) {
    display: none;
  }
`

export const HeaderItemMobile = styled.div`
  display: flex;
  align-items: center;
  @media (min-width: 1081px) {
    display: none;
  }
`
export const ImageContainer = styled.div`
  width: 250px;
  @media (max-width: 1080px) {
    width: 200px;
  }
`

export const SideMenuMobile = styled.div`
  position: fixed;
  overflow: hidden;
  z-index: 5;
  width: 100%;
  height: 100%;
  right: 0;
  background-color: rgba(0, 0, 0, 0.6);
  transform: scale(0);
  &.active {
    transform: scale(1);
  }
`

export const ContainerMobile = styled.div`
  position: absolute;
  z-index: 11;
  right: 0px;
  width: 238px;
  background-color: var(--color-yellow);
  transform: translateX(238px);
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 22px 0px;
  display: none;

  border-bottom-left-radius: 35px;

  a {
    text-decoration: none;
    font-style: normal;
    font-weight: 500;
    font-size: 22px;
    line-height: 33px;
    color: var(--color-white);
    margin-bottom: 32px;

    :nth-child(5) {
      padding: 0px 12px;
      background-color: var(--color-blue);
      border-radius: 500px;
    }
  }

  @media (min-width: 1081px) {
  }

  &.active {
    transform: translateX(0px);
    transition: 0.25s ease-in-out;
    display: flex;
  }
`

export const SideMenuSocial = styled.div``
