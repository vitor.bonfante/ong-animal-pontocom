import styled from 'styled-components'

export const Background = styled.div`
  width: 100%;
  padding-top: 50px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--color-blue);

  .title {
    width: 100%;
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 50px;
    line-height: 75px;
    color: var(--color-white);
  }

  .content {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    text-align: justify;
    color: var(--color-white);
  }

  .button-text {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    color: var(--color-white);

    line-height: 36px;
  }
`

export const Content = styled.div`
  width: 100%;
  max-width: 1100px;
  display: flex;
  @media (max-width: 1080px) {
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
`

export const ImageContainer = styled.div`
  width: 437px;
  height: 503px;
  margin-top: -51px;
  margin-left: 180px;
  @media (max-width: 1080px) {
    display: none;
  }
`

export const ImageMobile = styled.div`
  margin: 0;
  width: 235px;
  height: 288px;
  @media (min-width: 1080px) {
    display: none;
  }
`

export const Right = styled.div``

export const Left = styled.div`
  max-width: 450px;
  margin-left: 12px;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-right: 12px;

  > p {
    margin-bottom: 5px;
  }

  > a {
    text-decoration: none;
    margin-top: 5px;
    padding: 6px 24px;
    border-radius: 500px;
    background-color: var(--color-blue);
    border: 2px solid var(--color-white);
    cursor: pointer;
  }
`
