import Image from 'next/image'

import Dog from '../../../public/Animals/dog-blue.png'

import {
  Background,
  Left,
  Right,
  ImageContainer,
  Content,
  ImageMobile
} from './styles'

const RightDivAdopt: React.FC = () => {
  return (
    <Background id="about">
      <Content>
        <Left>
          <p className="title">Por que adotar?</p>
          <p className="content">
            Ao comprar um animal você acaba apoiando uma indústria que enxerga
            os animais como um produto. Adotar um animal diz não para essa
            prática cruel e transforma a vida do seu novo amigo, retirando ele
            de uma situação de rua difícil e dando a ele uma nova família e um
            lar seguro, cheio de amor e carinho, além de você ganhar um
            companheiro fiél.
          </p>
          <a target="_blank" href="https://www.instagram.com/amigofielong/">
            <p className="button-text">Quero adotar!</p>
          </a>
        </Left>
        <Right>
          <ImageContainer>
            <Image src={Dog} objectFit="cover" width="437" height="503" />
          </ImageContainer>
          <ImageMobile>
            <Image src={Dog} objectFit="cover" width="235" height="288" />
          </ImageMobile>
        </Right>
      </Content>
    </Background>
  )
}

export default RightDivAdopt
