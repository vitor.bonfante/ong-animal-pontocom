import Image from 'next/image'
import ReactSwipe from 'react-swipe'

import { Container, Content, ImageWrapper } from './styles'

import I1 from '../../../public/Carrousel/2.jpg'
import I2 from '../../../public/Carrousel/3.jpg'

const CarouselComp: React.FC = () => {
  let reactSwipeEl
  return (
    <div>
      <ReactSwipe
        className="carousel"
        childCount={2}
        swipeOptions={{ continuous: false }}
        ref={el => (reactSwipeEl = el)}
      >
        <div>
          <ImageWrapper>
            <Image src={I1} />
          </ImageWrapper>
        </div>
        <div>
          <ImageWrapper>
            <Image src={I2} />
          </ImageWrapper>
        </div>
      </ReactSwipe>
    </div>
  )
}

export default CarouselComp
