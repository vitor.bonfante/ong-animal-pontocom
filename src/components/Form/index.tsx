import Image from 'next/image'

import Woman from '../../../public/Form/woman.png'

import {
  Container,
  Content,
  FormMobile,
  Input,
  Left,
  Right,
  Select,
  WomanImage,
  WomanMobile
} from './styles'

const Form: React.FC = () => {
  async function handleEmail(e) {
    e.preventDefault()
    const formData = {}
    Array.from(e.currentTarget.elements).forEach(field => {
      //@ts-ignore
      if (!field.value) return
      //@ts-ignore
      formData[field.name] = field.value
    })
    fetch('/api/mail', {
      method: 'post',
      body: JSON.stringify(formData)
    })
      .then(res => {
        alert('Formulário enviado com sucesso!')
      })
      .catch(err => {
        alert('Houve um erro ao enviar o formulário, tente novamente!')
      })
  }

  return (
    <Container id="volunteering">
      <Content>
        <p className="title">Seja um voluntário</p>
        <form onSubmit={handleEmail} method="post" className="form">
          <Left>
            <Input name="name" placeholder="Nome" type="text" />
            <Input name="tel" placeholder="Telefone" type="text" />
            <Input
              name="birth_date"
              placeholder="Data de nascimento"
              type="text"
            />

            <button type="submit">
              <p>Quero ser um voluntário</p>
            </button>
          </Left>
          <Right>
            <Input name="email" placeholder="E-mail" type="email" />
            <Select name="gender" placeholder="Sexo">
              <optgroup>
                <option value="#">Sexo</option>
                <option value="Masculino">Masculino</option>
                <option value="Feminino">Feminino</option>
                <option value="Prefiro não dizer">Prefiro não dizer</option>
              </optgroup>
            </Select>
            <Select name="help" placeholder="Como gostaria de ajudar?">
              <optgroup>
                <option value="#">Como gostaria de ajudar?</option>
                <option value="Posso doar alimentos">
                  Posso doar alimentos
                </option>
              </optgroup>
            </Select>
          </Right>
          <WomanImage>
            <Image src={Woman} objectFit="cover" width="612" height="539" />
          </WomanImage>
        </form>
        <FormMobile>
          <Input name="name" placeholder="Nome" type="text" />
          <Input name="tel" placeholder="Telefone" type="text" />
          <Input
            name="birth_date"
            placeholder="Data de nascimento"
            type="text"
          />
          <Input name="email" placeholder="E-mail" type="email" />
          <Select name="gender" placeholder="Sexo">
            <optgroup>
              <option value="#">Sexo</option>
              <option value="Masculino">Masculino</option>
              <option value="Feminino">Feminino</option>
              <option value="Prefiro não dizer">Prefiro não dizer</option>
            </optgroup>
          </Select>
          <Select name="help" placeholder="Como gostaria de ajudar?">
            <optgroup>
              <option value="#">Como gostaria de ajudar?</option>
              <option value="Posso doar alimentos">Posso doar alimentos</option>
            </optgroup>
          </Select>
          <button type="submit">
            <p>Quero ser um voluntário</p>
          </button>
          <WomanMobile>
            <Image src={Woman} width="393" height="353" />
          </WomanMobile>
        </FormMobile>
      </Content>
    </Container>
  )
}

export default Form
