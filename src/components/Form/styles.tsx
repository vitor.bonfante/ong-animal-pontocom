import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  z-index: -1;
  padding-top: 50px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--color-yellow);

  .title {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 50px;
    line-height: 75px;
    color: var(--color-brown);
    margin-bottom: 14px;
  }

  .sub {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    line-height: 36px;
    color: var(--color-blue);
    margin-top: 35px;
    text-align: center;
  }
`
export const Content = styled.div`
  max-width: 1100px;
  width: 100%;

  > .form {
    display: flex;
    gap: 45px;

  }

  @media (max-width: 1080px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    > .form {
      display: none;
    }
    padding-left: 12px;
    padding-right: 12px;
  }
`

export const FormMobile = styled.form`
  @media (min-width: 1080px) {
    display: none;
  }
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  padding-left: 12px;
  padding-right: 12px;

  > input,
  select {
    margin-bottom: 33px;
    width: 100%;
  }

  > button {
    background: var(--color-brown);
    border-radius: 40px;
    padding: 9px 22px;
    outline: none;
    border: none;

    cursor: pointer;

    > p {
      font-family: 'Poppins';
      font-style: normal;
      font-weight: 700;
      font-size: 19px;
      line-height: 28px;
      color: var(--color-yellow);
    }
  }
`

export const Input = styled.input`
  max-width: 315px;
  width: 100%;
  border-radius: 40px;
  border: 2px solid var(--color-brown);
  padding: 9px 22px;
  display: flex;
  align-items: center;
  background-color: var(--color-yellow);
  outline: none;
  ::-webkit-input-placeholder {
    /* Edge */
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    line-height: 28px;
    color: var(--color-brown);
  }

  :-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    color: var(--color-brown);
    line-height: 28px;
  }

  ::placeholder {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    color: var(--color-brown);
    line-height: 28px;
  }
`

export const Select = styled.select`
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;

  max-width: 315px;
  width: 100%;
  border-radius: 40px;
  border: 2px solid var(--color-brown);
  padding: 6px 22px;
  display: flex;
  align-items: center;
  background-color: var(--color-yellow);
  outline: none;

  font-family: 'Poppins';
  font-style: normal;
  font-weight: 500;
  font-size: 15px;

  color: var(--color-brown);

  > optgroup {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    color: var(--color-brown);
    line-height: 28px;
  }
`

export const Left = styled.div`
  display: flex;
  flex-direction: column;
  gap: 34px;
  

  > button {
    background: var(--color-brown);
    border-radius: 40px;
    padding: 9px 22px;
    outline: none;
    border: none;
    cursor: pointer;

    > p {
      font-family: 'Poppins';
      font-style: normal;
      font-weight: 700;
      font-size: 19px;
      line-height: 28px;
      color: var(--color-yellow);
    }
  }
`

export const Right = styled.div`
  display: flex;
  flex-direction: column;
  gap: 34px;
`
export const WomanImage = styled.div`
  margin-top: -180px;
  margin-right: -150px;
  display: flex;
  height: 539px;
  width: 612px;
`

export const WomanMobile = styled.div`
  display: flex;
  height: 353px;
  width: 393px;
`
