import React from 'react'
import Head from 'next/head'
import Header from '../components/Header'
import LeftDiv from '../components/LeftDiv'
import RightDiv from '../components/RightDiv'
import Footer from '../components/Footer'
import Partners from '../components/Partners'
import Form from '../components/Form'
import RightDivAdopt from '../components/RightDivAdopt'
import CarouselComp from '../components/Carousel'

const Home: React.FC = () => {
  return (
    <div>
      <Head>
        <title>Amigo Fiel ONG</title>
      </Head>
      <main>
        <Header />
        <CarouselComp />
        <RightDivAdopt />
        <LeftDiv />
        <RightDiv />
        <Partners />
        {/* <Form /> */}
        <Footer />
      </main>
    </div>
  )
}

export default Home
